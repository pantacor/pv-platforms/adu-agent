# Device Update (DU) Agent Container

Pantavisor offers a simple yet powerful userspace layer on top of the Linux Kernel whose sole purpose is to provide a runtime and a deploy platform that allows to make up a Linux Embedded system in its entirety out of a personal mix of containers.

Naturally, the Device Update agent is also implemented as a container that you can add to any Pantavisor enabled device. Once done, you can can onboard your Pantavisor device and use Device Update to deploy software updates at scale. The main components of the DU agent container are:

* [Device Update service with PVControl content handler](https://github.com/pantacor/iot-hub-device-update/tree/pv/container-ci): the DU agent
* [Delivery Optimization service](https://github.com/microsoft/do-client.git): dependecy of ADU agent used to download the updates
* [pvcontrol](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/blob/master/files/usr/bin/pvcontrol): used by the ADU PVControl Handler for communication betwen the agent and Pantavisor

**NOTE:** Containers are not natively run by Docker but by Pantavisor, which offers a lightweight container engine to do so. The dockerfile is only used to define the container root file system.

## Get Started

This guide objective is to show off how Device Update can be used to ship containerized updates from the Azure cloud to your Pantavisor devices. It is not meant to be a complete solution for a production environment.

For this example, we will install a new container onto the device through Azure IOT Hub cloud.

### Install the tools in your host computer

1. [pvr](https://docs.pantahub.com/install-pvr/): will be used interact with your device from your host computer.
2. [pvr2adu](iot-hub-device-update/docs/pvcontrol-reference/pvr2adu.md): download images and prepare updates.

### Update your Pantavisor device with Device Update

**NOTE:** To make guide as simple as possible, we are going to start from an already existing Pantavisor-enabled device. See how to get to this point in this [get-started guide](https://docs.pantahub.com/before-you-begin/).

1. Set up [Device Update](https://docs.microsoft.com/en-us/azure/iot-hub-device-update/create-device-update-account).

2. Install our Device Update Agent container and configuration in your Pantavisor-enabled device:

```
pvr app add --from registry.gitlab.com/pantacor/pv-platforms/adu-agent:amd64
echo -e "connection_string=<your-connection-string>\naduc_manufacturer=Pantacor\naduc_model=Toaster" > _config/adu-agent/etc/adu/adu-conf.txt
pvr add .
pvr commit
pvr post
```

4. Turn on your device and [check connection](https://docs.microsoft.com/en-us/azure/iot-hub/monitor-iot-hub) to Azure IOT Hub.

5. [Clone](https://docs.pantahub.com/clone-your-system/#clone-your-systems) the current running revision of your device in your host computer.

6. Make changes in the pvr checkout.

For this example, we are goint to install a new NGINX container from [DockerHub](https://hub.docker.com/_/nginx), but any container that is compatible with the device architecture should work.

```
cd my-checkout
pvr app add --from nginx:stable-alpine webserver
pvr add .
pvr commit
```

7. Convert the pvr checkout into DU format.

For this, we need the information to generate the [Device Update manifest](https://docs.microsoft.com/en-us/azure/iot-hub-device-update/update-manifest):

```
Usage: pvr2adu [options] export <provider> <name> <version> <manufacturer> <model> <output>
Export the current pvr checkout into DU compatible format
```

For example, to set the manifest to be consumed by our previously configured device, we would do:

```
pvr2adu export Pantacor Toaster 1.0 Fabrikam Toaster ../out
```

8. [Import](https://docs.microsoft.com/en-us/azure/iot-hub-device-update/import-update#import-an-update) the resulting manifest and tarball. 

9. [Deploy](https://docs.microsoft.com/en-us/azure/iot-hub-device-update/deploy-update) the update.

## TODO

* update to adu agent 0.8.0
* allow consuming pvr exports so tarballs are not that big
* study if we can avoid copying /usr/lib so the container is not that big
* cross-compile arm targets instead of natively compile them
